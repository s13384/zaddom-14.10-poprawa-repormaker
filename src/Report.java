
public class Report {

	private String className;
	private int numberOfKids;
	
	Report(String className, int numberOfKids)
	{
		this.className = className;
		this.numberOfKids = numberOfKids;
	}
	public String getClassName() {
		return className;
	}
	public int getNumberOfKids() {
		return numberOfKids;
	}
}
