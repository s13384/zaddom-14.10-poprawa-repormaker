
public class ReportMaker implements IVisitor {

	private int numberOfClients;
	private int numberOfOrders;
	private int numberOfProducts;
	private String report;
	
	public int getNumberOfClients() {
		return numberOfClients;
	}
	public void setNumbers(ClientGroup cG) {

		numberOfClients = 0;
		numberOfOrders = 0;
		numberOfProducts = 0;
		cG.accept(this);
	}
	public int getNumberOfOrders() {
		return numberOfOrders;
	}
	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	@Override
	public void visit(IVisitable v) {

		Report tmpReport=v.giveReport();
		if (tmpReport.getClassName()=="ClientGroup")
		{
			numberOfClients = tmpReport.getNumberOfKids();
		}
		else if (tmpReport.getClassName()=="Client")
		{
			numberOfOrders += tmpReport.getNumberOfKids();
		}
		else if (tmpReport.getClassName()=="Order")
		{
			numberOfProducts += tmpReport.getNumberOfKids();
		}
		else if (tmpReport.getClassName()=="Product")
		{
			
		}
		
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}

}
