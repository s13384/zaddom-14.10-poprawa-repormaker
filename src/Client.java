import java.util.ArrayList;


public class Client implements IVisitable {

	private String number;
	private ArrayList<Order> orders;
	
	Client()
	{
		orders = new ArrayList<Order>();
	}
	@Override
	public void accept(IVisitor visitor) {
		
		visitor.visit(this);
		for (Order o : orders)
		{
			o.accept(visitor);
		}

	}

	@Override
	public Report giveReport() {

		return new Report("Client", orders.size());
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public void addOrder(Order order)
	{
		orders.add(order);
	}
	public ArrayList<Order> getOrders()
	{
		return orders;
	}

}
