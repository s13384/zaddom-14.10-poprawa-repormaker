
public class Main {

	public static void main(String[] args) {
		
		ClientGroup clientGroup = new ClientGroup("grupa");
		Client c1 = new Client() ,c2 = new Client(),c3 = new Client(),c4 = new Client(), c5 = new Client();
		clientGroup.addClient(c1);
		clientGroup.addClient(c2);
		clientGroup.addClient(c3);
		clientGroup.addClient(c4);
		clientGroup.addClient(c5);
		
		Order o1=new Order(), o2=new Order(), o3=new Order(), o4=new Order(), o5=new Order();
		c1.addOrder(o1);
		c2.addOrder(o2);
		c3.addOrder(o3);
		c4.addOrder(o4);
		c4.addOrder(o5);
		
		Product p1 = new Product("1",100), p2 = new Product("2",100), p3 = new Product("3",100), 
				p4 = new Product("4",100), p5 = new Product("5",100), p6 = new Product("6",100);
		
		o1.addProduct(p1);
		o2.addProduct(p2);
		o3.addProduct(p3);
		o4.addProduct(p4);
		o4.addProduct(p5);
		o5.addProduct(p6);
		
		ReportMaker repoMaker = new ReportMaker();
		repoMaker.setNumbers(clientGroup);
		repoMaker.setNumbers(clientGroup);
		repoMaker.setNumbers(clientGroup);
		
		System.out.println("Liczba klientow : " + repoMaker.getNumberOfClients());
		System.out.println("Liczba zamowien : " + repoMaker.getNumberOfOrders());
		System.out.println("Liczba produktow : " + repoMaker.getNumberOfProducts());
	}

}
