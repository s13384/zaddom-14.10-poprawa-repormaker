
public class Product implements IVisitable {

	private String name;
	private double price;
	
	Product(String name, double price){
		this.name=name;
		this.price=price;
	}
	@Override
	public void accept(IVisitor visitor) {
		
		visitor.visit(this);

	}

	@Override
	public Report giveReport() {
		
		return new Report("Product",0);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
