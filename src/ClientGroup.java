import java.util.ArrayList;


public class ClientGroup implements IVisitable {

	private String groupName;
	private ArrayList<Client> clients;
	
	ClientGroup(String groupName)
	{
		this.groupName = groupName;
		clients =  new ArrayList<Client>();
	}
	@Override
	public void accept(IVisitor visitor) {
		visitor.visit(this);
		for (Client c : clients)
		{
			c.accept(visitor);
		}

	}

	@Override
	public Report giveReport() {
		
		return new Report("ClientGroup",clients.size());
	}

	public String getMemberName() {
		return groupName;
	}

	public void setMemberName(String memberName) {
		this.groupName = memberName;
	}
	public void addClient(Client client)
	{
		clients.add(client);
	}
	public ArrayList<Client> getClients()
	{
		return clients;
	}

}
